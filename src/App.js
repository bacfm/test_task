import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import ContentListComponent from './components/FilesList';
import ToolBar from './components/ToolBar';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ToolBar />
        <Route exact path="/:path" component={ContentListComponent}/>
          <Route exact path="/" component={ContentListComponent}/>
      </div>
    );
  }
}

export default App;
