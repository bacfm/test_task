export const get_files = (res) => ({
   type: 'GET_FILES',
   payload: {files: res.files, folders: res.folders}
});

export const add_item_to_list = (type, path, name) => ({
   type: 'ADD_ELEMENT_TO_LIST',
   item: {type: type, path: path, name: name}
});

export const remove_item_from_list = (path) => ({
    type: 'REMOVE_ITEM_FROM_LIST',
    path: path
})