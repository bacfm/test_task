import { get_files } from '../actions';

export const fetch_files = (path) => {
    if(!path || path === '/'){
       path="public";
    }
    return (dispatch) => {
        return fetch(`http://localhost:8080/files/${path}`)
            .then(res => res.json())
            .then(res => dispatch(get_files(res)))
            .catch(err => console.log(err));
    }
}

export const add_new_dir = (name, path) => {
    const body = { dir: path + "/" + name };
    console.log(body);
    return (dispatch) => {
        return fetch('http://localhost:8080/new-dir', {
            headers: {
                'Content-type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(body)
        })
            .then(res => {
                if(res.status === 200){
                    dispatch(fetch_files(path.slice(1)))
                } else {
                    console.log('ERROR');
                }
            })
            .catch((err) => console.log(err))
    }
}

export const upload_file = (file, path) => {
    if(!path || path === '/'){
        path="/public";
    }
    console.log(path);
    return (dispatch) => {
        return fetch(`http://localhost:8080/files${path}`, {
            method: 'POST',
            body: file
        })
            .then(res => {
                console.log(res);
                if(res.status === 200){
                    dispatch(fetch_files(path.slice(1)))
                } else {
                    console.log('ERROR');
                }
            })
            .catch(err => console.log(err));
    }
}

export const remove_dir = (dirName, path) => {
    const body = { dir: path };
    return (dispatch) => {
        return fetch('http://localhost:8080/remove-dir', {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(res => {
                if(res.status === 200){
                    dispatch(fetch_files(path.replace(dirName, '')))
                }
            })
            .catch(err => console.log(err))
    }
}

export const remove_file = (fileName, path) => {
    console.log(path, fileName)
    const body = { element: "/" + path };
    return (dispatch) => {
        return fetch('http://localhost:8080/remove-file', {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(res => {
                if(res.status === 200){
                    dispatch(fetch_files(path.replace(fileName, '')))
                }
            })
            .catch(err => console.log(err))
    }
}