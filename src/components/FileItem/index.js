import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { remove_item_from_list, add_item_to_list } from '../../actions';
import './index.css';

class FileItemComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            checked: false
        }
        this.onCheckChange = this.onCheckChange.bind(this);
    }
    onCheckChange(ev){
        this.setState({checked: !this.state.checked});
        const { current_path, name } = this.props;
        let path;
        if(current_path === '/'){
            path = name;
        }else{
            path = current_path + "/" + name;
        }
        if(ev.target.value === 'false') {
            this.props.addToList(this.props.type, path, this.props.name)
        } else {
            this.props.removeFromList(path);
        }
    }
    render(){
        const { current_path, name, type, size, modified } = this.props;
        let path;
        if(current_path === '/'){
            path = current_path + name;
        }else{
            path = current_path + "/" + name;
        }
        return(
            <tr className="file-item th">
                <th scope="col" className="table-checkbox"><input type="checkbox" onChange={this.onCheckChange} value={this.state.checked}/></th>
                <th scope="col" className="table-file">{type === 'folder' ? (<Link className="folder-link" to={path}></Link>) : null}<div className="file_name"><div className={type === 'file' ? 'file-icon' : 'folder-icon'}></div>{name}</div></th>
                <th scope="col" className="table-size"><div className="file_size">Размер: {size}</div></th>
                <th scope="col" className="table-date"><div className="last_change">Дата модификации: {modified.slice(0, 10)}</div></th>
            </tr>
        );
    }
}

const mapDispatch = (dispatch) => {
    return {
        addToList: (type, path, name) => dispatch(add_item_to_list(type, path, name)),
        removeFromList: (path) => dispatch(remove_item_from_list(path))
    }
}

export default connect(null, mapDispatch)(FileItemComponent);