import React, { Component } from 'react';
import { connect } from 'react-redux';
import {fetch_files} from "../../api";
import FileItemComponent from '../FileItem';

class ContentListComponent extends Component {
    componentWillMount(){
        console.log(this.props.path);
        this.props.GetFiles(this.props.path);
    }
    render(){
        if(this.props.state.folders && this.props.state.files) {
            const folders = this.props.state.folders.map((f, idx) => (<FileItemComponent key={idx} name={f.name} size={f.size} modified={f.date} type={'folder'}  current_path={this.props.path}/>));
            const files = this.props.state.files.map((f, idx) => (<FileItemComponent key={idx} name={f.name} size={f.size} modified={f.date} type={'file'} current_path={this.props.path}/>));
            return(
                <table className="table table-hover">
                    <tbody>
                    {folders}
                    {files}
                    </tbody>
                </table>
            );
        }else{
            return (<div></div>)
        }
    }
}
const mapState = (state, { match }) => {
    let path;
    if(window.location.pathname === '/'){
        path = '';
    } else {
        path = match.params.path;
    }
    console.log(match);
    return {
        state: state.fileSystem,
        path:  path
    }
}
const mapDispatch = (dispatch) => {
    return {
        GetFiles: (path) => dispatch(fetch_files(path))
    }
}
export default connect(mapState, mapDispatch)(ContentListComponent);