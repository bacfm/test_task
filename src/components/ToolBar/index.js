import React, { Component } from 'react';
import { connect } from 'react-redux';
import { add_new_dir, upload_file, remove_dir, remove_file, fetch_files } from '../../api';
import './style.css';

class ToolBar extends Component {
    constructor(props){
        super(props);
        this.state = {
            modalNewFolder: false,
            folderName: '',
            file: ''
        };
        this.onShowModalFolder = this.onShowModalFolder.bind(this);
        this.onFolderNameChange = this.onFolderNameChange.bind(this);
        this.onSaveNewFolder = this.onSaveNewFolder.bind(this);
        this.onUploadFile = this.onUploadFile.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }
    onShowModalFolder(ev){
        ev.preventDefault();
        this.setState({modalNewFolder: !this.state.modalNewFolder});
    }
    onFolderNameChange(ev){
        this.setState({folderName: ev.target.value})
    }
    onSaveNewFolder(ev){
        ev.preventDefault();
        this.props.newDir(this.state.folderName, window.location.pathname);
        this.onShowModalFolder(ev);
    }
    onUploadFile(ev){
        let file = new FormData();
        file.append('file', ev.target.files[0]);
        this.props.onUpload(file, window.location.pathname);
        ev.target.value = '';
    }
    onDelete(ev){
        ev.preventDefault();
        this.props.filesList.forEach((el) => {
            if(el.type === 'file'){
                this.props.onRemoveFile(el.name, el.path)
            } else if(el.type === 'folder'){
                this.props.onRemoveDir(el.name, el.path)
            } else {
                console.log("What is it? 0_o");
            }
        });
    }
    render(){
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <ul className="navbar-nav">
                    <li className="nav-item nav-link">
                        <button className="btn btn-secondary" onClick={this.onDelete}>Удалить</button></li>
                    <li className="nav-item nav-link">
                        <button className="btn btn-secondary" onClick={this.onShowModalFolder}>Добавить папку</button></li>
                    <li className="nav-item nav-link">
                        <label className="btn btn-secondary">Добавить файл</label>
                        <input type="file" value={this.state.file} onChange={this.onUploadFile}/>
                    </li>
                </ul>
                {this.state.modalNewFolder ? (
                    <div className="modal" tabIndex="-1" role="dialog">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">Добавить папку</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" onClick={this.onShowModalFolder}>&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <input type="text" className="form-control" placeholder="Название папки" onChange={this.onFolderNameChange}/>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-primary" onClick={this.onSaveNewFolder}>Создать</button>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.onShowModalFolder}>Отмена
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : null}
            </nav>
        );
    }
}

const mapState = (state) => {
    return {
        filesList: state.deleteList
    }
}

const mapDispatch = (dispatch) => {
    return {
        newDir: (name, path) => dispatch(add_new_dir(name, path)),
        onUpload: (file, path) => dispatch(upload_file(file, path)),
        onRemoveFile: (fileName, path) => dispatch(remove_file(fileName, path)),
        onRemoveDir: (dirName, path) => dispatch(remove_dir(dirName, path)),
        getFiles: (path) => dispatch(fetch_files(path))
    }
}

export default connect(mapState, mapDispatch)(ToolBar);