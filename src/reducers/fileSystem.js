export const fileSystem = (state = [], action) => {
    if(action.type === 'GET_FILES'){
        return  action.payload;
    } else {
        return state;
    }
};
