import { combineReducers } from 'redux';
import { fileSystem } from './fileSystem';
import { deleteList } from './itemsToDelete';


export default combineReducers({
    fileSystem,
    deleteList
})