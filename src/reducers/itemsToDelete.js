export const deleteList = (state = [], action) => {
    if(action.type === 'ADD_ELEMENT_TO_LIST'){
        return [...state, action.item]
    }else if (action.type === 'REMOVE_ITEM_FROM_LIST'){
        const { path } = action;
        const itemIndex = state.findIndex(elem => elem.path === path);
        return [
            ...state.slice(0, itemIndex),
            ...state.slice(itemIndex + 1)
        ]
    }
    return state;
}