const express = require('express');
const app = express();
const fs = require('fs');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        let path;
        if(req.params.path === 'public'){
            path = __dirname + "/public/"
        } else {
            path = __dirname + "/public/" + req.params.path;
        }
        if(!fs.existsSync(path)){
            fs.mkdir(path, (err) => {
                if(err){
                    console.log(err);
                }
            })
        }
        cb(null, path);
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname);
    }
});
const upload = multer({ storage });
const cors = require('cors');
const bodyParser = require('body-parser');




app.use(cors());
app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.get('/files/:folder', (req, res) => {
    let path;
   if(!req.params.folder || req.params.folder == 'public'){
     path = __dirname + "/public";
   } else {
       path = __dirname + "/public/" + req.params.folder;
   }
    let files_names = [];
    let folders_names = [];
    fs.readdir(path, (err, items) => {
        if(items) {
            items.forEach((item) => {
                const state = fs.statSync(`${path}/${item}`)
                if (state.isFile()) {
                    files_names.push({name: item, size: state.size, date: state.mtime})
                } else {
                    folders_names.push({name: item, size: state.size, date: state.mtime});
                }
            });
            res.status(200).json({message: 'OK', files: files_names, folders: folders_names});
        }
    });
});

app.post('/files/:path', upload.single('file'), (req, res) => { res.status(200).json('ok'); });
app.post('/new-dir', (req, res) => {
    const dir_path = __dirname + '/public/' + req.body.dir;
   fs.mkdir(dir_path, (err) => {
       if(err) {
           res.status(400).json({message: 'Can not to make new folder'});
       }else{
           res.status(200).json({message: 'OK'})
       }
   })
});

app.delete('/remove-dir', (req, res) => {
    const dir_path = __dirname + '/public/' + req.body.dir;
    removeDir(dir_path, res);
});

app.delete('/remove-file', (req, res) => {
    console.log(req.body);
   const element_path = __dirname + '/public' + req.body.element;
   console.log(element_path);
   fs.unlink(element_path);
    res.status(200).json("ok");
});


//function for remove dir

function removeDir(path, res) {
  fs.readdir(path, (err, files) => {
      if(err) {
          res.status(400).json(err);
      } else {
          if(files.length === 0){
              fs.rmdir(path, (err) => {
                  if(err) res.status(400).json(err);
              });
          } else {
              files.forEach(file => {
                  let file_path = path + "/" + file;
                  let stat = fs.statSync(file_path);
                  if(stat.isFile()){
                      fs.unlinkSync(file_path);
                  }else{
                      removeDir(file_path);
                  }
              });
          }
      }
  });
  if(fs.existsSync(path)){
      res.status(200).json('ok');
  }
};

app.listen(8080, () => {
    console.log('You are listening on 8080');
});